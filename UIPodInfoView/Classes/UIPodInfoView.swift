
//  Copyright © 2017 Apple. All rights reserved.


import UIKit

let kBundleName = "UIPodInfoView"
let kBundle = "bundle"
let kUIAppFonts = "UIAppFonts"

public protocol UIPodInfoViewDelegate{
    
    func podInfoViewClicked(podId : Int)
    
    func podSurchargeClicked(podId : Int)
    
}

 @IBDesignable public class UIPodInfoView : UIView {
    
    @IBOutlet var podView: UIView!

    @IBOutlet weak var lblName : UILabel!
    
    @IBOutlet var lblAddress: UILabel!
    
    @IBOutlet var btnPodSurcharge: UIButton!
    
    @IBOutlet var lblTime: UILabel!
    
    public var podID : Int! = 0
    
    public var delegate:UIPodInfoViewDelegate!
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadBundle()
 
        self.addSubview(podView)
        podView.frame = self.bounds
        
        self.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.addGestureRecognizer(tap)

    }
    
    func loadBundle() {
        
        let podBundle = Bundle(for: self.classForCoder)
        
        if let bundleURL = podBundle.url(forResource: kBundleName, withExtension: kBundle) {
            
            if let bundle = Bundle(url: bundleURL) {
                
                var arrFontNames : NSArray =  bundle.infoDictionary?[kUIAppFonts] as! NSArray
                
                for fontName in arrFontNames as! [String] {
                    let fontURL = bundle.path(forResource: fontName, ofType: "")!
                    UIPodInfoView.loadFont(fontName:fontName,baseFolderPath:fontURL)
                }
                
                UINib(nibName: kBundleName, bundle: bundle).instantiate(withOwner: self, options: nil)[0] as! UIView
            }
        }
        
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
       
        if( delegate != nil)
        {
           delegate!.podInfoViewClicked(podId: podID)
        }
    }
    
    @IBAction func podSurchargeClicked(_ sender: Any) {
        
        if( delegate != nil)
        {
            delegate!.podSurchargeClicked(podId: podID)
        }
    }
    

    static func loadFont(fontName: String, baseFolderPath: String) -> Bool {
        
        let fontUrl = NSURL(fileURLWithPath: baseFolderPath)
        if let inData = NSData(contentsOf: fontUrl as URL) {
            var error: Unmanaged<CFError>?
            
            let cfdata = CFDataCreate(nil, inData.bytes.assumingMemoryBound(to: UInt8.self), inData.length)
            if let provider = CGDataProvider(data: cfdata!) {
                
                let font = CGFont(provider)
    
                if (!CTFontManagerRegisterGraphicsFont(font, &error)) {
                        print("Failed to load font: \(error)")
                    
                    return true
                }
            }
        }
        return false
    }

}
