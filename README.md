# UIPodInfoView

[![CI Status](http://img.shields.io/travis/Vinayagamoorthi2606/UIPodInfoView.svg?style=flat)](https://travis-ci.org/Vinayagamoorthi2606/UIPodInfoView)
[![Version](https://img.shields.io/cocoapods/v/UIPodInfoView.svg?style=flat)](http://cocoapods.org/pods/UIPodInfoView)
[![License](https://img.shields.io/cocoapods/l/UIPodInfoView.svg?style=flat)](http://cocoapods.org/pods/UIPodInfoView)
[![Platform](https://img.shields.io/cocoapods/p/UIPodInfoView.svg?style=flat)](http://cocoapods.org/pods/UIPodInfoView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UIPodInfoView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "UIPodInfoView"
```

## Author

Vinayagamoorthi2606, Vinayagamoorthi@versatile-soft.com

## License

UIPodInfoView is available under the MIT license. See the LICENSE file for more info.
