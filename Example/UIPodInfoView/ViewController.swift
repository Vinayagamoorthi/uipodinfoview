//
//  ViewController.swift
//  UIPodInfoView
//
//  Created by Vinayagamoorthi2606 on 04/06/2017.
//  Copyright (c) 2017 Vinayagamoorthi2606. All rights reserved.
//

import UIKit
import UIPodInfoView


class ViewController: UIViewController {
    
    @IBOutlet var infoPodView: UIPodInfoView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        infoPodView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}


extension ViewController : UIPodInfoViewDelegate {
  
    func podInfoViewClicked(podId : Int)
    {
        print("podInfoViewClicked at home \(podId)")
    }
    
    func podSurchargeClicked(podId : Int)
    {
        print("podSurchargeClicked at home \(podId)")
    }
    
}
